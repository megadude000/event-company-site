﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace zzzz.Models
{
    public class OrderForView
    {
        [Display(Name = "ID")]
        public int OrderID { get; set; }
        [Display(Name = "Коментарий")]
        [DataType(DataType.MultilineText)]
        public string Comment { get; set; }
        [Display(Name = "Стоимость")]
        public int Cost { get; set; }
        [Display(Name = "Внесенная сумма")]
        public int Currentammount { get; set; }
        [Display(Name = "Статус")]
        public string Status { get; set; }
        [Display(Name = "Дата регистрации")]
        public DateTime Regtime { get; set; }
        [Display(Name = "Тип")]
        public string EventType { get; set; }

        [Display(Name = "ID покупателя")]
        public int CustomerID { get; set; }
        [Display(Name = "ID сценария")]
        public int ScriptID { get; set; }

        public Customer customer { get; set; }

    }
}