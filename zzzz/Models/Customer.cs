﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace zzzz.Models
{
    public class Customer
    {
        [Key]
        [Display(Name = "ID")]
        public int CustomerID { get; set; }
        [Display(Name = "Связан с логином")]
        public string LoginName { get; set; }
        [Required]
        [Display(Name = "Имя покупателя")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Email покупателя")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Тел. номер")]
        public string PhoneNumber { get; set; }
    }
}