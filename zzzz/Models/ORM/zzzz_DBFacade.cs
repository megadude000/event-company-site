﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace zzzz.Models.ORM
{
    public class zzzz_DBFacade : DbContext
    {
        static zzzz_DBFacade()
        {
            Database.SetInitializer(
                new MyInitializer()
            );
            
        }

        public DbSet<Image> Images { get; set; }
        public DbSet<Script> Scripts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Customer> Customers { get; set; }
    }
}