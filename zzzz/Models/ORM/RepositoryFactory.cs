﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using zzzz.Models.ORM.Repository_Implementations;
using zzzz.Models.ORM.Repository_Interfaces;

namespace zzzz.Models.ORM
{
    public static class RepositoryFactory
    {
        public static IImageRep MakeImageRepository(zzzz_DBFacade dbContext)
        {
            return new ImageRep(dbContext);
        }

        public static IScriptRep MakeScriptRepository(zzzz_DBFacade dbContext)
        {
            return new ScriptRep(dbContext);
        }

        public static IOrderRep MakeOrderRepository(zzzz_DBFacade dbContext)
        {
            return new OrderRep(dbContext);
        }

        public static ICustomerRep MakeCustomerRepository(zzzz_DBFacade dbContext)
        {
            return new CustomerRep(dbContext);
        }
    }
}