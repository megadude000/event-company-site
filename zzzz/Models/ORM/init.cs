﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace zzzz.Models.ORM
{
    public class MyInitializer : DropCreateDatabaseAlways<zzzz_DBFacade>
    {
        protected override void Seed(zzzz_DBFacade context)
        {
            Script sc = new Script();
            sc.eventType = "type";
            sc.scriptBody="body";
            sc.scriptDur=10;
            sc.scriptname="name";
            context.Scripts.Add(sc);
        }
    }
}