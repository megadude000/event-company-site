﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using zzzz.Models.ORM.Repository_Interfaces;

namespace zzzz.Models.ORM.Repository_Implementations
{
    public class CustomerRep: BasicRep<Customer>, ICustomerRep
    {
        public CustomerRep(zzzz_DBFacade dbContext)
            :   base( dbContext, dbContext.Customers )
        {
        }
    }
}