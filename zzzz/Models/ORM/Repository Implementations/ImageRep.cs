﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using zzzz.Models.ORM.Repository_Interfaces;

namespace zzzz.Models.ORM.Repository_Implementations
{
    public class ImageRep : BasicRep<Image>, IImageRep
    {
        public ImageRep(zzzz_DBFacade dbContext)
            : base(dbContext, dbContext.Images)
        {
        }
    }
}