﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace zzzz.Models.ORM.Repository_Implementations
{
    public class BasicRep<T> where T : class
    {
        protected BasicRep(zzzz_DBFacade dbContext, DbSet<T> dbSet)
        {
            this.dbContext = dbContext;
            this.dbSet = dbSet;
        }


        protected zzzz_DBFacade GetDBContext()
        {
            return this.dbContext;
        }


        protected DbSet GetDBSet()
        {
            return this.dbSet;
        }


        public void Add(T obj)
        {
            dbSet.Add(obj);
        }


        public void Commit()
        {
            dbContext.ChangeTracker.DetectChanges();
            dbContext.SaveChanges();
        }


        public IQueryable<T> LoadAll()
        {
            return dbSet;
        }


        public T Load(int id)
        {
            return dbSet.Find(id);
        }

        public T Load(int? id)
        {
            return dbSet.Find(id);
        }

        public T Remove (T t)
        {
            return dbSet.Remove(t);
        }


        private zzzz_DBFacade dbContext;
        private DbSet<T> dbSet;
    }
}