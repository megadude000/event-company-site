﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace zzzz.Models.ORM.Repository_Interfaces
{
    public interface IRepository<T> where T : class
    {
        T Load(int id);

        T Load(int? id);

        T Remove(T t);

        IQueryable<T> LoadAll();

        void Add(T t);

        void Commit();
    }
}