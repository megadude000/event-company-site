﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace zzzz.Models
{
    public class Script
    {
        
        [Key]
        [Display(Name = "ID")]
        public int id { get; set; }

        [Display(Name = "Название сценария")]
        public string scriptname { get; set; }

        [Display(Name = "Тип ивента сценария")]
        public string eventType { get; set; }

        [Display(Name = "Тело сценария")]
        public string scriptBody { get; set; }

        [Display(Name = "Длительность")]
        public int scriptDur { get; set; }

    }
}