﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using fit;
using zzzz.Models;
using zzzz.Controllers;
using fitlibrary;
using System.Web.Mvc;

namespace zzzz.Tests
{
    public class FitTest : ColumnFixture
    {
        
        public int ScriptID()
        {
            ScriptController controller = new ScriptController();

            Script scr = new Script();
            scr.eventType = "type";
            scr.scriptBody = "body";
            scr.scriptDur = 10;
            scr.scriptname = "Somename";
            controller.Create(scr);


            var result = controller.Details(2) as ViewResult;
            var script = (Script)result.ViewData.Model;

            return script.id;
        }

    }
}