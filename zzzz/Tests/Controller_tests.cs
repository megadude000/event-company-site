﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using zzzz.Controllers;
using System.Web.Mvc;
using zzzz.Models;

namespace zzzz.Tests
{
    [TestClass]
    public class Controller_tests
    {
        [TestMethod]
        public void TestCreate()
        {
            var controller = new ScriptController();

            Script scr = new Script();
            scr.eventType = "type";
            scr.scriptBody = "body";
            scr.scriptDur = 10;
            scr.scriptname = "Somename";
            controller.Create(scr);
            

            var result = controller.Details(2) as ViewResult;
            var product = (Script)result.ViewData.Model;
            Assert.AreEqual("Somename", product.scriptname);
        }

        [TestMethod]
        public void TestCreateProperly()
        {
            var controller = new ScriptController();

            Script scr = new Script();
            scr.eventType = "type";
            scr.scriptBody = "body";
            scr.scriptDur = 10;
            scr.scriptname = "name";
            controller.Create(scr);

            var result = controller.Details(2) as ViewResult;
            var product = (Script)result.ViewData.Model;
            Assert.AreNotEqual("somename", product.scriptname);
        }

        [TestMethod]
        public void TestEdit()
        {
            var controller = new ScriptController();

            Script scr = new Script();
            scr.eventType = "type";
            scr.scriptBody = "body";
            scr.scriptDur = 10;
            scr.scriptname = "Newname";
            scr.id = 1;
            controller.Edit(scr);
 
            var result = controller.Details(1) as ViewResult;
            var product = (Script)result.ViewData.Model;
            Assert.AreEqual("Newname", product.scriptname);
        }

        //[TestMethod]
        //public void TestIncorrectEdit()
        //{
        //    var controller = new ScriptController();

        //    var result = (RedirectToRouteResult)controller.Details(-1);
        //    Assert.AreEqual("Index", result.RouteName.ToString());
        //}

        [TestMethod]
        public void IndexViewResultNotNull()
        {
            var controller = new ScriptController();

            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void IndexStringInViewbag()
        {
            var controller = new ScriptController();

            ViewResult result = controller.Details(1) as ViewResult;

            Assert.AreEqual("SomeMessage", result.ViewBag.Message);
        }
        [TestMethod]
        public void TestReadyForView()
        {
            var controller = new ScriptController();

            var result = controller.Details(1) as ViewResult;
            var scr = (Script)result.ViewData.Model;
            Assert.AreEqual(1, scr.id);
        }

    }
}