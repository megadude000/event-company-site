﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace zzzz.Tests
{
    [TestClass]
    public class SeleniumTest
    {
        IWebDriver driver;
        string BaseURL = "http://localhost:6152/";
        [TestInitialize]
        public void TestSetup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl(BaseURL);
        }
        [TestMethod]
        public void Click()
        {
            driver.FindElement(By.XPath("/html/body/div/div[1]/div/div[1]/ul/li[1]/a")).Click();
            System.Threading.Thread.Sleep(3000);
        }
        [TestCleanup]
        public void TestCleanup()
        {
            driver.Quit();
        }
    }
}