﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(zzzz.Startup))]
namespace zzzz
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
