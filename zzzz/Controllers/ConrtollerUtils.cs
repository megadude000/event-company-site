﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using zzzz.Models.ORM.Repository_Interfaces;

namespace zzzz.Controllers
{
    static class ControllerUtils
    {
        public static T ResolveObjectById<T>(IRepository<T> repository, int objectID)
            where T : class
        {
            T result = repository.Load(objectID);
            if (result == null)
                throw new SystemException(typeof(T).Name + " #" + objectID + " is unresolved");
            return result;
        }
    }
}