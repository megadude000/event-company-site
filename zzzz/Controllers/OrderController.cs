﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using zzzz.Models;
using zzzz.Models.ORM;
using zzzz.Models.ORM.Repository_Interfaces;

namespace zzzz.Controllers
{
    public class OrderController : Controller
    {
        private IOrderRep OrderRepository;
        private zzzz_DBFacade dbContext;

        public zzzz_DBFacade GetDBContext()
        {
            if (dbContext == null)
                dbContext = new zzzz_DBFacade();

            return dbContext;
        }

        public OrderController()
        {
            this.OrderRepository = RepositoryFactory.MakeOrderRepository(GetDBContext());
        }

        private Order ResolveScript(int id)
        {
            return ControllerUtils.ResolveObjectById(OrderRepository, id);
        }

        private OrderForView temp = new OrderForView();

        // GET: /Order/
        [Authorize(Users="admin")]
        public ActionResult Index()
        {
            return View(OrderRepository.LoadAll().ToList());
        }

        [Authorize(Users = "admin")]// GET: /Order/Details/5
        public ActionResult Details(int? id)
        {
            ICustomerRep CustomerRepository = RepositoryFactory.MakeCustomerRepository(GetDBContext());
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = OrderRepository.Load(id);
            Customer customer = CustomerRepository.Load(order.CustomerID);
            temp.Comment = order.Comment;
            temp.Cost = order.Cost;
            temp.Currentammount = order.Currentammount;
            temp.customer = customer;
            temp.CustomerID = order.CustomerID;
            temp.EventType = order.EventType;
            temp.OrderID = order.OrderID;
            temp.Regtime = order.Regtime;
            temp.ScriptID = order.ScriptID;
            temp.Status = order.Status;

            if (order == null)
            {
                return HttpNotFound();
            }
            return View(temp);
        }

        // GET: /Order/Create
        public ActionResult Create()
        {
            ICustomerRep CustomerRepository = RepositoryFactory.MakeCustomerRepository(GetDBContext());
            if (User.Identity.IsAuthenticated == false)
            {
                OrderForView order = new OrderForView();
                order.Regtime = DateTime.Now;
                temp = order;
                return View(order);
            }
            if (User.Identity.Name == "admin")
            {
                OrderForView order = new OrderForView();
                order.Regtime = DateTime.Now;
                temp = order;
                return View(order);
            }
            if (User.Identity.Name != "admin" && User.Identity.IsAuthenticated)
            {
                var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
                var user = manager.FindById(User.Identity.GetUserId());
                Customer customerInfo = CustomerRepository.Load(user.CustomerID);
                OrderForView order = new OrderForView();
                order.customer = customerInfo;
                order.Regtime = DateTime.Now;
                order.CustomerID = customerInfo.CustomerID;
                temp = order;
                return View(order);
            }
            return View();
        }

        // POST: /Order/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="OrderID,Comment,Cost,Currentammount,Status,Regtime,EventType,CustomerID,ScriptID")] Order order, OrderForView forview)
        {
            if (ModelState.IsValid)
            {
                ICustomerRep CustomerRepository = RepositoryFactory.MakeCustomerRepository(GetDBContext());
                if (User.Identity.Name == "admin")
                {
                    CustomerRepository.Add(forview.customer);
                    CustomerRepository.Commit();

                    order.CustomerID = forview.customer.CustomerID;
                    order.Status = "New";

                    OrderRepository.Add(order);
                    OrderRepository.Commit();
                    return RedirectToAction("Index");
                }
                if (!User.Identity.IsAuthenticated)
                {
                    CustomerRepository.Add(forview.customer);
                    CustomerRepository.Commit();

                    order.CustomerID = forview.customer.CustomerID;
                    order.Status = "New";

                    OrderRepository.Add(order);
                    OrderRepository.Commit();
                    return RedirectToAction("Thanks", "Home");
                }
                if (User.Identity.Name != "admin" && User.Identity.IsAuthenticated)
                {
                    var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
                    var user = manager.FindById(User.Identity.GetUserId());
                    order.CustomerID = user.CustomerID;
                    order.Status = "New";

                    OrderRepository.Add(order);
                    OrderRepository.Commit();
                    return RedirectToAction("Thanks", "Home");
                }

            }

            return View(order);
        }

        [Authorize(Users = "admin")]// GET: /Order/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = OrderRepository.Load(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: /Order/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="OrderID,Comment,Cost,Currentammount,Status,Regtime,EventType,CustomerID,ScriptID")] Order order)
        {
            if (ModelState.IsValid)
            {
                Order ord = ResolveScript(order.OrderID);
                    ord.Comment = order.Comment;
                    ord.Cost = order.Cost;
                    ord.Currentammount = order.Currentammount;
                    ord.CustomerID = order.CustomerID;
                    ord.EventType = order.EventType;
                    ord.ScriptID = order.ScriptID;
                    ord.Status = order.Status;
                    OrderRepository.Commit();
                return RedirectToAction("Index");
            }
            return View(order);
        }

        [Authorize(Users = "admin")]// GET: /Order/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = OrderRepository.Load(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: /Order/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = OrderRepository.Load(id);
            OrderRepository.Remove(order);
            OrderRepository.Commit();
            return RedirectToAction("Index");
        }

    }
}
