﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using zzzz.Models;
using zzzz.Models.ORM;
using zzzz.Models.ORM.Repository_Interfaces;

namespace zzzz.Controllers
{
    public class ImageController : Controller
    {
        private IImageRep ImageRepository;
        private zzzz_DBFacade dbContext;

        public zzzz_DBFacade GetDBContext()
        {
            if (dbContext == null)
                dbContext = new zzzz_DBFacade();

            return dbContext;
        }

        public ImageController()
        {
            this.ImageRepository = RepositoryFactory.MakeImageRepository(GetDBContext());
        }

        //
        [Authorize(Users = "admin")]// GET: /Image/
        public ActionResult Index()
        {
            return View(ImageRepository.LoadAll().ToList());
        }
        [Authorize(Users = "admin")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Image model, HttpPostedFileBase file)
        {
            if (file!=null)
            {
                // code for saving the image file to a physical location.
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/dwn"), fileName);
                file.SaveAs(path);

                // prepare a relative path to be stored in the database and used to display later on.
                path = Url.Content(Path.Combine("~/Content/dwn", fileName));
                Image img = new Image();
                img.path = path;

                ImageRepository.Add(img);
                ImageRepository.Commit();

                return RedirectToAction("Index");

            }
            return RedirectToAction("Index");
        }
        [Authorize(Users = "admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Image img = ImageRepository.Load(id);
            if (img == null)
            {
                return HttpNotFound();
            }
            return View(img);
        }

        // POST: /Script/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Image img = ImageRepository.Load(id);
            ImageRepository.Remove(img);
            ImageRepository.Commit();
            return RedirectToAction("Index");
        }
	}
}