﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using zzzz.Models;
using zzzz.Models.ORM;
using zzzz.Models.ORM.Repository_Interfaces;

namespace zzzz.Controllers
{
    public class ScriptController : Controller
    {
        private IScriptRep ScriptRepository;
        private zzzz_DBFacade dbContext;

        public zzzz_DBFacade GetDBContext()
        {
            if (dbContext == null)
                dbContext = new zzzz_DBFacade();

            return dbContext;
        }

        public ScriptController()
        {
            this.ScriptRepository = RepositoryFactory.MakeScriptRepository(GetDBContext());
        }

        private Script ResolveScript(int id)
        {
            return ControllerUtils.ResolveObjectById(ScriptRepository, id);
        }


        [Authorize(Users = "admin")]// GET: /Script/
        public ActionResult Index()
        {
            return View(ScriptRepository.LoadAll().ToList());
        }

        [Authorize(Users = "admin")]// GET: /Script/Details/5
        public ActionResult Details(int? id)
        {
            if (id < 1)
                return RedirectToAction("Index");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Script script = ScriptRepository.Load(id);
            if (script == null)
            {
                return HttpNotFound();
            }

            ViewBag.Message = "SomeMessage";
            return View(script);
        }

        [Authorize(Users = "admin")]// GET: /Script/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Script/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,scriptname,eventType,scriptBody,scriptDur")] Script script)
        {
            if (ModelState.IsValid)
            {
                ScriptRepository.Add(script);
                ScriptRepository.Commit();
                return RedirectToAction("Index");
            }

            return View(script);
        }

        [Authorize(Users = "admin")]// GET: /Script/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Script script = ScriptRepository.Load(id);
            if (script == null)
            {
                return HttpNotFound();
            }
            return View(script);
        }

        // POST: /Script/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,scriptname,eventType,scriptBody,scriptDur")] Script script)
        {
            if (script.id < 1)
                return RedirectToAction("Index");

            if (ModelState.IsValid)
            {
                Script scr = ResolveScript(script.id);

                scr.scriptname = script.scriptname;
                scr.eventType = script.eventType;
                scr.scriptBody = script.scriptBody;
                scr.scriptDur = script.scriptDur;

                ScriptRepository.Commit();
                return RedirectToAction("Index");
            }
            return View(script);
        }

        [Authorize(Users = "admin")]// GET: /Script/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Script script = ScriptRepository.Load(id);
            if (script == null)
            {
                return HttpNotFound();
            }
            return View(script);
        }

        // POST: /Script/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Script script = ScriptRepository.Load(id);
            ScriptRepository.Remove(script);
            ScriptRepository.Commit();
            return RedirectToAction("Index");
        }

    }
}
