﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using zzzz.Models;
using zzzz.Models.ORM;
using zzzz.Models.ORM.Repository_Interfaces;

namespace zzzz.Controllers
{
    public class CustomerController : Controller
    {
         private ICustomerRep CustomerRepository;
        private zzzz_DBFacade dbContext;

        public zzzz_DBFacade GetDBContext()
        {
            if (dbContext == null)
                dbContext = new zzzz_DBFacade();

            return dbContext;
        }

        public CustomerController()
        {
            this.CustomerRepository = RepositoryFactory.MakeCustomerRepository(GetDBContext());
        }

        private Customer ResolveCustomer(int id)
        {
            return ControllerUtils.ResolveObjectById(CustomerRepository, id);
        }

        [Authorize(Users = "admin")]// GET: /Customer/
        public ActionResult Index()
        {
            return View(CustomerRepository.LoadAll().ToList());
        }

        [Authorize(Users = "admin")]// GET: /Customer/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = CustomerRepository.Load(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        public ActionResult DetailsFromManage(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = CustomerRepository.Load(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        [Authorize(Users = "admin")]// GET: /Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="CustomerID,LoginID,Name,Email,PhoneNumber")] Customer customer)
        {
            List<Customer> list = CustomerRepository.LoadAll().ToList();
            var ifexist = list.Where(s => s.LoginName == User.Identity.Name);
            if (ifexist.Any())
            { return RedirectToAction("Index"); }
            else
            {
                if (ModelState.IsValid)
                {
                    customer.LoginName = User.Identity.Name;
                    CustomerRepository.Add(customer);
                    CustomerRepository.Commit();

                    var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
                    var user = manager.FindByName(customer.LoginName);
                    user.CustomerID = customer.CustomerID;
                    manager.Update(user);
                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }


        public ActionResult CreateFromManage()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateFromManage([Bind(Include = "CustomerID,LoginID,Name,Email,PhoneNumber")] Customer customer)
        {
            List<Customer> list  = CustomerRepository.LoadAll().ToList();
            var ifexist = list.Where(s => s.LoginName == User.Identity.Name);
            if (ifexist.Any())
            { return RedirectToAction("ManageAccount", "Account"); }
            else
            {
                if (ModelState.IsValid)
                {
                    customer.LoginName = User.Identity.Name;
                    CustomerRepository.Add(customer);
                    CustomerRepository.Commit();

                    var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
                    var user = manager.FindById(User.Identity.GetUserId());
                    user.CustomerID = customer.CustomerID;
                    manager.Update(user);
                    return RedirectToAction("ManageAccount", "Account");
                }
            }

            return RedirectToAction("ManageAccount", "Account");
        }

        [Authorize(Users = "admin")]// GET: /Customer/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = CustomerRepository.Load(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }


        // POST: /Customer/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="CustomerID,LoginID,Name,Email,PhoneNumber")] Customer customer)
        {
            List<Customer> list  = CustomerRepository.LoadAll().ToList();
            var ifexist = list.Where(s => s.LoginName == User.Identity.Name);
            if (ifexist.Any())
            { return RedirectToAction("Index"); }
            else
            {
                if (ModelState.IsValid)
                {
                    Customer cus = ResolveCustomer(customer.CustomerID);
                        cus.Email = customer.Email;
                        cus.LoginName = customer.LoginName;
                        cus.Name = customer.Name;
                        cus.PhoneNumber = customer.PhoneNumber;
                    CustomerRepository.Commit();
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
        }
        public ActionResult EditFromManage(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = CustomerRepository.Load(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditFromManage([Bind(Include = "CustomerID,LoginID,Name,Email,PhoneNumber")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                Customer cus = ResolveCustomer(customer.CustomerID);
                    cus.Email = customer.Email;
                    cus.LoginName = customer.LoginName;
                    cus.Name = customer.Name;
                    cus.PhoneNumber = customer.PhoneNumber;
                CustomerRepository.Commit();
                return RedirectToAction("ManageAccount", "Account");
            }
            return RedirectToAction("ManageAccount", "Account");
        }


        [Authorize(Users = "admin")]// GET: /Customer/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = CustomerRepository.Load(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: /Customer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Customer customer = CustomerRepository.Load(id);
            var currentUserId = User.Identity.GetUserId();
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var user = manager.FindByName(customer.LoginName);
            user.CustomerID = 0;
            manager.Update(user);
            CustomerRepository.Remove(customer);
            CustomerRepository.Commit();
            return RedirectToAction("Index");
        }
    }
}
