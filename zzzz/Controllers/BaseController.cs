﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using zzzz.Models.ORM;

namespace zzzz.Controllers
{
    public class BaseController
    {
        public zzzz_DBFacade GetDBContext()
        {
            if (dbContext == null)
                dbContext = new zzzz_DBFacade();

            return dbContext;
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (disposing && dbContext != null)
                dbContext.Dispose();
        }

        private zzzz_DBFacade dbContext;
    }
}